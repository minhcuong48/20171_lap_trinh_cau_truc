/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithmicdecomposition;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class AlgorithmicDecomposition {

    static String pot = "Am tra";
    static String greenTea = "tra xanh";
    static String blackTea = "tra den";
    static String boilingWater = "nuoc soi";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        produceTea();
        System.out.printf("Ket qua: %s\n", pot);
    }
    
    public static String fetchGreenTea() {
        return greenTea;
    }
    
    public static String fetchBlackTea() {
        return blackTea;
    }
    
    public static void pourSpoonTeaIntoPot(String kindOfTea) {
        pot += "\n\tco 1 muong " + kindOfTea;
    }
    
    public static void putTeaInPot() {
        for (int i=0; i<3; i++) {
            pourSpoonTeaIntoPot(fetchBlackTea());
        }
    }
    
    public static boolean potHasTea() {
        if (pot.contains("co 1 muong")) return true;
        return false;
    }
    
    public static void addBoilingWater() {
        pot += "\n\tco nuoc soi";
    }
    
    public static boolean potHasBoilingWater() {
        if (pot.contains("nuoc soi")) return true;
        return false;
    } 
    
    public static void wait(int min) {
        pot += "\n\tTra da ngam sau " + min + " phut\n";
    }
    
    public static void produceTea() {
        putTeaInPot();
        if (potHasTea()) addBoilingWater();
        if (potHasBoilingWater()) wait(10);
    }
    
}
