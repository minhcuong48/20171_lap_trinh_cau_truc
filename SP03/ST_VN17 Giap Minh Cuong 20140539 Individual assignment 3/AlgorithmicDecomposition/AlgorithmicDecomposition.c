/**
 *
 * @author GiapMinhCuong-20140539
 */

/*
Co 1 bien pot toan cuc
Cac doi so ban dau bao gom: so luong muong tra xanh, so luong muong tra den, thoi gian doi
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum Tea {	// co 2 loai tra
	BLACK_TEA, GREEN_TEA
}	Tea;

typedef struct {
	Tea tea;
}	Spoon;	// muong tra: 1 don vi do luong tra

typedef struct {
	int hasBoilingWater;
	int numberOfGreenTeaSpoons;
	int numberOfBlackTeaSpoons;
	int waitTime;
}	Pot;	// am tra

// bien toan cuc
Pot pot;

void initPot() {
	pot.hasBoilingWater = 0;
	pot.numberOfBlackTeaSpoons = 0;
	pot.numberOfGreenTeaSpoons = 0;
	pot.waitTime = 0;
}
  
Tea fetchGreenTea() {
    return GREEN_TEA;
}
    
Tea fetchBlackTea() {
    return BLACK_TEA;
}
    
void pourSpoonTeaIntoPot(Tea tea) {
	if (tea == GREEN_TEA) {
		pot.numberOfGreenTeaSpoons += 1;
	}
	else if (tea == BLACK_TEA) {
		pot.numberOfBlackTeaSpoons += 1;
	}
}
    
void putTeaInPot(Tea tea, int number) {
    for (int i=0; i<number; i++) {
        pourSpoonTeaIntoPot(tea);
    }
}
    
int potHasTea() {
    if (pot.numberOfBlackTeaSpoons > 0 || pot.numberOfGreenTeaSpoons > 0) return 1;
    return 0;
}
    
void addBoilingWater() {
    if (potHasTea() == 0) {	// neu pot chua co tra thi khong do nuoc
    	return;
    }

    pot.hasBoilingWater = 1;
}
    
int potHasBoilingWater() {
    if (pot.hasBoilingWater == 0) return 0;
    return 1;
} 
    
void waitForTea(int min) {
    if (potHasTea() == 0 || potHasBoilingWater() == 0) {	// neu pot chua co tra hoac nuoc thi ko can doi
    	return;
    }
    if (min <= 0) {	// kiem tra hop le
    	return;
    }
    pot.waitTime = min;
}
    
void produceTea(int numberOfGreenTeaSpoons, int numberOfBlackTeaSpoons, int waitTime) {
	putTeaInPot(GREEN_TEA, numberOfGreenTeaSpoons);
    putTeaInPot(BLACK_TEA, numberOfBlackTeaSpoons);
    if (potHasTea()) addBoilingWater();
    if (potHasBoilingWater()) waitForTea(waitTime);
}

void showResult() {
	printf("Am tra co\n");
	printf("\t%d muong tra xanh\n", pot.numberOfGreenTeaSpoons);
	printf("\t%d muong tra den\n", pot.numberOfBlackTeaSpoons);
	if (potHasBoilingWater()) {
		printf("\tnuoc nong\n");
	}
	if (pot.waitTime > 0) {
		printf("Tra da ngam sau %d phut\n", pot.waitTime);
	}
}

int main(int argc, char* argv[]) {
	initPot();
    produceTea(2, 3, 10);
    showResult();
    return 0;
}
