/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectorienteddecomposition;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class Pot {
    private List<Spoon> spoons = new ArrayList<>();
    private BoilingWater boilingWater = null;
    private int waitTime = 0;
    
    private int numberOfGreenTeaSpoons() {
        int number = 0;
        for (int i=0; i<spoons.size(); i++) {
            if (spoons.get(i).getKindOfTea() == GreenTea.class) {
                number++;
            }
        }
        return number;
    }
    
    private int numberOfBlackTeaSpoons() {
        int number = 0;
        for (int i=0; i<spoons.size(); i++) {
            if (spoons.get(i).getKindOfTea() == BlackTea.class) {
                number++;
            }
        }
        return number;
    }
    
    public void pourSpoonTea(Spoon spoon) {
        spoons.add(spoon);
    }
    
    public void putTea(Tea tea, int numberOfSpoons) {
        for (int i=0; i<numberOfSpoons; i++) {
            pourSpoonTea(new Spoon(tea));
        } 
    }
    
    public void addBoilingWater(BoilingWater water) {
        if (spoons.isEmpty()) { // neu am tra chua co tra thi ko do nuoc nong
            return;
        }
        boilingWater = water;
    }
    
    public void wait(int minutes) {
        // neu chua co tra hoac nuoc nong thi khong can phai doi
        if (spoons.isEmpty() || boilingWater == null) {
            return;
        }
        // check input hop le
        if (minutes <= 0) {
            return;
        }
        
        waitTime = minutes;
    }

    @Override
    public String toString() {
        String result = "Am tra co:\n"
                + "\t" + numberOfGreenTeaSpoons() + " muong tra xanh\n"
                + "\t" +numberOfBlackTeaSpoons() + " muong tra den";
        if (boilingWater != null) {
            result += "\n\tnuoc soi";
        }
        
        if (waitTime > 0) {
            result += "\nTra da ngam sau " + waitTime + " phut";
        }
        
        return result;
    }
    
    
}
