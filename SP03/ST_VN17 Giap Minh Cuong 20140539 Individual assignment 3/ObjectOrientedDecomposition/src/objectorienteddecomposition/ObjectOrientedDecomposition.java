/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectorienteddecomposition;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class ObjectOrientedDecomposition {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Pot pot = new Pot();
        pot.putTea(new GreenTea(), 5);
        pot.putTea(new BlackTea(), 3);
        pot.addBoilingWater(new BoilingWater());
        pot.wait(15);
        System.out.printf("Ket qua: %s\n", pot);
    }
    
}
