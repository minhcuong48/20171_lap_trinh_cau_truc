/**
 *
 * @author GiapMinhCuong-20140539
 */

/*
Ban dau can co 1 cai pot rong. moi function deu co 1 input la pot va output la cai pot do sau khi bien doi
Output cua ham nay dung lam input cua ham khac
Cac doi so ban dau:1 cai pot, so luong muong tra xanh, so luong muong tra den, thoi gian doi
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum Tea {	// co 2 loai tra
	BLACK_TEA, GREEN_TEA
}	Tea;

typedef struct {
	Tea tea;
}	Spoon;	// muong tra: 1 don vi do luong tra

typedef struct {
	int hasBoilingWater;
	int numberOfGreenTeaSpoons;
	int numberOfBlackTeaSpoons;
	int waitTime;
}	Pot;	// am tra

Pot newPot() {
	Pot pot;
	pot.hasBoilingWater = 0;
	pot.numberOfBlackTeaSpoons = 0;
	pot.numberOfGreenTeaSpoons = 0;
	pot.waitTime = 0;
	return pot;
}
  
Tea fetchGreenTea() {
    return GREEN_TEA;
}
    
Tea fetchBlackTea() {
    return BLACK_TEA;
}
    
Pot pourSpoonTeaIntoPot(Pot pot, Tea tea) {
	if (tea == GREEN_TEA) {
		pot.numberOfGreenTeaSpoons += 1;
	}
	else if (tea == BLACK_TEA) {
		pot.numberOfBlackTeaSpoons += 1;
	}
	return pot;
}
    
Pot putTeaInPot(Pot pot, Tea tea, int number) {
    for (int i=0; i<number; i++) {
        pot = pourSpoonTeaIntoPot(pot, tea);
    }
    return pot;
}
    
int potHasTea(Pot pot) {
    if (pot.numberOfBlackTeaSpoons > 0 || pot.numberOfGreenTeaSpoons > 0) return 1;
    return 0;
}
    
Pot addBoilingWater(Pot pot) {
    if (potHasTea(pot) == 0) {	// neu pot chua co tra thi khong do nuoc
    	return pot;
    }

    pot.hasBoilingWater = 1;
    return pot;
}
    
int potHasBoilingWater(Pot pot) {
    if (pot.hasBoilingWater == 0) return 0;
    return 1;
} 
    
Pot waitForTea(Pot pot, int min) {
    if (potHasTea(pot) == 0 || potHasBoilingWater(pot) == 0) {	// neu pot chua co tra hoac nuoc thi ko can doi
    	return pot;
    }
    if (min <= 0) {	// kiem tra hop le
    	return pot;
    }
    pot.waitTime = min;
    return pot;
}
    
Pot produceTea(Pot pot, int numberOfGreenTeaSpoons, int numberOfBlackTeaSpoons, int waitTime) {
    pot = putTeaInPot(pot, GREEN_TEA, numberOfGreenTeaSpoons);
    pot = putTeaInPot(pot, BLACK_TEA, numberOfBlackTeaSpoons);
    if (potHasTea(pot)) pot = addBoilingWater(pot);
    if (potHasBoilingWater(pot)) pot = waitForTea(pot, waitTime);
    return pot;
}

void showResult(Pot pot) {
	printf("Am tra co\n");
	printf("\t%d muong tra xanh\n", pot.numberOfGreenTeaSpoons);
	printf("\t%d muong tra den\n", pot.numberOfBlackTeaSpoons);
	if (potHasBoilingWater(pot)) {
		printf("\tnuoc nong\n");
	}
	if (pot.waitTime > 0) {
		printf("Tra da ngam sau %d phut\n", pot.waitTime);
	}
}

int main(int argc, char* argv[]) {
	Pot pot = newPot();
    pot = produceTea(pot, 2, 3, 10);
    showResult(pot);
    return 0;
}
