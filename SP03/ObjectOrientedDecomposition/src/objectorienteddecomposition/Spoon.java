/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectorienteddecomposition;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class Spoon {
    private Tea tea;

    public Spoon(Tea tea) {
        this.tea = tea;
    }
    
    public Class getKindOfTea() {
        return tea.getClass();
    }
    
    public String getName() {
        if (tea instanceof GreenTea) {
            return "1 muong tra xanh";
        }
        else if (tea instanceof BlackTea) {
            return "1 muon tra den";
        }
        return null;
    }
}
