/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functiondecomposition;

/**
 *
 * @author GiapMinhCuong-20140539
 */
public class FunctionDecomposition {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String pot = produceTea();
        System.out.printf("Ket qua: %s\n", pot);
    }
    
    public static String fetchGreenTea() {
        return "tra xanh";
    }
    
    public static String fetchBlackTea() {
        return "tra den";
    }
    
    public static String pourSpoonTeaIntoPot(String pot, String kindOfTea) {
        pot += "\n\tco 1 muong " + kindOfTea;
        return pot;
    }
    
    public static String putTeaInPot(String pot) {
        for (int i=0; i<3; i++) {
            pot = pourSpoonTeaIntoPot(pot, fetchGreenTea());
        }
        return pot;
    }
    
    public static String addBoilingWater(String pot) {
        pot += "\n\tco nuoc soi";
        return pot;
    }
    
    public static String wait(String pot) {
        pot += "\n\tTra da ngam";
        return pot;
    }
    
    public static String produceTea() {
        String pot = "Am tra";
        pot = putTeaInPot(pot);
        pot = addBoilingWater(pot);
        pot = wait(pot);
        return pot;
    }
}
